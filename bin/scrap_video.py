#!/usr/bin/env python3

import json
import moviepy.editor
from argparse import ArgumentParser
from pathlib import Path
from urllib.parse import urlparse, parse_qs
from urllib.error import HTTPError
from urllib import request

ROOT_PATH = Path("./public")

FILENAMES = [
    "metadata.xml",
    "shapes.svg",
    "cursor.xml",
    "panzooms.xml",
    "presentation_text.json",
    "captions.json",
    "deskshare.xml",
    "presentation/deskshare.png",
    "slides_new.xml",
    "deskshare/deskshare.webm",
    "video/webcams.webm",
]

class Meeting:

    def __init__(self, meeting_url):
        """Set domain and id from url"""
        self.id = ""
        url_parsed = urlparse(meeting_url)
        self.domain = url_parsed.hostname
        if "meetingId" in url_parsed.query:
            self.id = parse_qs(url_parsed.query)['meetingId'][0]
            self.path = ROOT_PATH / "presentation" / self.id
            self.url_dir = "https://{0}/presentation/{1}".format(self.domain, self.id)
        else:
            raise Exception("No id found in url, Meeting cannot be retrieved")

    @property
    def presentations(self):
        """Get presentations"""
        presentation_text = {}
        presentations = []
        with open(self.path / "presentation_text.json") as f:
            presentation_text = json.load(f)
            presentations = list(presentation_text.keys())
        return presentations

    def make_directories(self):
        """Create directories in root path"""
        if not self.path.exists():
            self.path.mkdir()
        audio_dir = self.path / "audio"
        video_dir = self.path / "video"
        deskshare_dir = self.path / "deskshare"
        presentation_dir = self.path / "presentation"
        for directory in [audio_dir, video_dir, deskshare_dir, presentation_dir]:
            if not directory.exists():
                directory.mkdir()

    def retrieve_files(self):
        """Retrieve meeting files"""
        for filename in FILENAMES:
            print("Retrieving {0}".format(filename))
            try:
                request.urlretrieve(
                    "{0}/{1}".format(self.url_dir, filename),
                    self.path / filename,
                    )
            except HTTPError:
                print('File not found')

        # Get presentation files
        for presentation in self.presentations:
            presentation_directory = self.path / "presentation/{0}".format(presentation)
            if not presentation_directory.exists():
                presentation_directory.mkdir()
            try:
                request.urlretrieve(
                    "{0}/presentation/{1}/slide-1.png".format(self.url_dir, presentation),
                    presentation_directory / "slide-1.png".format(presentation),
                )
            except HTTPError:
                print('File not found')

    def extract_audio(self):
        """Extract video from webcams"""
        video = moviepy.editor.VideoFileClip("{0}/video/webcams.webm".format(self.path))
        audio = video.audio
        audio.write_audiofile("{0}/audio/webcams.mp3".format(self.path))

def retrieve_meeting(args):
    if "meeting_url" in args:
        meeting = Meeting(args.meeting_url)
        meeting.make_directories()
        meeting.retrieve_files()

def extract_audio(args):
    if "meeting_url" in args:
        meeting = Meeting(args.meeting_url)
        meeting.extract_audio()


def main():
    """Main function for script"""
    parser = ArgumentParser(description='Scrap video from BBB')
    subparsers = parser.add_subparsers(help='sub-command help')

    # Retrieve command
    parser_retrieve = subparsers.add_parser('retrieve', help='Retrieve video from a BBB instance')
    # parser_retrieve.add_argument('meeting_url', type=str, help='URL of the presentation')
    parser_retrieve.set_defaults(func=retrieve_meeting)

    # Audio command
    parser_audio = subparsers.add_parser('audio', help='Get audio from a meeting webcam video')
    # parser_audio.add_argument('meeting_url', type=str, help='URL of the presentation')
    parser_audio.set_defaults(func=extract_audio)

    parser.add_argument('meeting_url', type=str, help='URL of the presentation')

    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
